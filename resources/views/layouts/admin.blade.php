<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">

    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f5f5f5; /* Light grey background color */
        }

        nav {
            background-color: #d0d4d7; /* Blue background color for the navigation bar */
            padding: 10px 0;
            text-align: center;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        li {
            display: inline-block;
            margin-right: 20px;
        }

        a {
            text-decoration: none;
            color: black; /* White text color for links */
            font-weight: bold;
            font-size: 16px;
        }

        .main {
            max-width: 800px;
            margin: 20px auto;
            padding: 20px;
            background-color: #ffffff; /* White background color for the main content */
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        footer {
            background-color: #d0d4d7; 
            color: #090606; 
            text-align: center;
            padding: 20px 0; 
            position: fixed;
            bottom: 0;
            width: 100%;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <title>@yield('title', 'App store')</title>
</head>
<body>
    <nav>
        <ul>
            <li><a href="{{ route('home.index') }}">Catalogue</a></li>
            <li><a href="{{ route('categories.index') }}">Gestion des categories</a></li>
            <li><a href="{{ route('produits.index') }}">Gestion des produits</a></li>
            <li><a href="{{ route('home.panier') }}">Mon panier</a></li>
        </ul>
    </nav>
    <div class="main">
        @yield('content')
    </div>
    <footer>
        &copy; OFPPT 2024 
    </footer>
</body>
</html>
