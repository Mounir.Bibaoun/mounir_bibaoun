<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>

    </style>
</head>
<body>
    @extends('layouts.admin')
    @section('title', 'Modifier un produit')
    @section('content')
        <div class="container my-4">
            <h1 class="display-4 text-center">Modifier le produit</h1>
    
            <form action="{{ route('produits.update', ['produit' => $pro->id]) }}" method="POST" enctype="multipart/form-data" class="mx-auto">
                @csrf
                @method('PUT')
    
                <div class="mb-3 row">
                    <label for="designation" class="col-sm-3 col-form-label">Designation</label>
                    <div class="col-sm-9">
                        <input type="text" name="designation" id="designation" class="form-control" value="{{ old('designation', $pro->designation) }}">
                    </div>
                </div>
    
                <div class="mb-3 row">
                    <label for="prix_u" class="col-sm-3 col-form-label">Prix unitaire</label>
                    <div class="col-sm-9">
                        <input type="text" name="prix_u" id="prix_u" class="form-control" value="{{ old('prix_u', $pro->prix_u) }}">
                    </div>
                </div>
    
                <div class="mb-3 row">
                    <label for="quantite_stock" class="col-sm-3 col-form-label">Quantité en stock</label>
                    <div class="col-sm-9">
                        <input type="text" name="quantite_stock" id="quantite_stock" class="form-control" value="{{ old('quantite_stock', $pro->quantite_stock) }}">
                    </div>
                </div>
    
                <div class="mb-3 row">
                    <label for="categorie_id" class="col-sm-3 col-form-label">Categorie ID</label>
                    <div class="col-sm-9">
                        <input type="text" name="categorie_id" id="categorie_id" class="form-control" value="{{ old('categorie_id', $pro->categorie_id) }}">
                    </div>
                </div>
    
                <div class="mb-3 row">
                    <label for="photo" class="col-sm-3 col-form-label">Photo</label>
                    <div class="col-sm-9">
                        <input type="file" name="photo" id="photo" class="form-control">
                    </div>
                </div>
    
                <div class="mb-3 row">
                    <div class="col-sm-9 offset-sm-3">
                        <input type="submit" value="Modifier" class="btn btn-primary">
                    </div>
                </div>
            </form>
    
            @if($errors->any())
                <div class="alert alert-danger mt-4">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    @endsection
    
</body>
</html>
