<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">

  <title>Document</title>
</head>
<body>

@extends('layouts.admin')
@section('title', 'Gestion des produits')
@section('content')
    <div class="container mt-4">
      <h1>rechercher des produits</h1>
      <form action="{{ route('produits.index') }}" method="GET" class="mb-3">
        @csrf
        <div class="row">
            <div class="col-md-3 mb-3">
                <label for="categorie_id" class="form-label">Search by Category ID</label>
                <input type="text" name="categorie_id" class="form-control" placeholder="Enter category ID..."
                    value="{{ old('categorie_id', $categorie_id) }}">
            </div>
            <div class="col-md-3 mb-3">
                <label for="prix_max" class="form-label">Search by Max Price</label>
                <input type="text" name="prix_max" class="form-control" placeholder="Enter max price..."
                    value="{{ old('prix_max', $prix_max) }}">
            </div>
            <div class="col-md-3 mb-3">
                <label for="prix_min" class="form-label">Search by Min Price</label>
                <input type="text" name="prix_min" class="form-control" placeholder="Enter min price..."
                    value="{{ old('prix_min', $prix_min) }}">
            </div>
            <div class="col-md-3 mb-3">
                <label for="designation" class="form-label">Search by Designation</label>
                <input type="text" name="designation" class="form-control" placeholder="Enter designation..."
                    value="{{ old('designation', $designation) }}">
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-2">Search</button>
    </form>
    
        <h1>Liste des produits</h1>
    <div><h4>total des produits : {{ $nbr_produits}}</h4></div>

        <a href="{{ route('produits.create') }}" class="btn btn-primary mb-3">Ajouter un nouveau produit</a>

        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Designation</th>
                    <th>prix_u</th>
                    <th>quantite_stock</th>
                    <th>categorie_id</th>
                    <th colspan="3">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($produits as $pro)
                    <tr>
                      
                        <td>{{ $pro->id }}</td>
                        <td>{{ $pro->designation }}</td>
                        <td>{{ $pro->prix_u }}</td>
                        <td>{{ $pro->quantite_stock }}</td>
                        <td>{{ $pro->categorie_id }}</td>
                        <td><a href="{{ route('produits.show', ["produit" => $pro->id]) }}"
                                class="btn btn-primary">Details</a></td>
                        <td><a href="{{ route('produits.edit', ["produit" => $pro->id]) }}"
                                class="btn btn-success">Modifier</a></td>
                        <td>
                            <form action="{{ route('produits.destroy', ["produit" => $pro->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('Voulez-vous supprimer ce produit?')">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
       
        <div class="d-flex justify-content-center">
                  {{ $produits->links() }}
         </div>
@endsection
</body>
</html>
