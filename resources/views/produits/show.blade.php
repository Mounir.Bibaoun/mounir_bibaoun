@extends('layouts.admin')

@section('title', 'Détail d\'un produit')

@section('content')
    <a href="{{ route('produits.index') }}" enctype="multipart/form-data" class="btn btn-primary mb-3">Retourner vers la liste des produits</a>
    
    <div class="card">
        <div class="card-header">
            <h1 class="card-title">Détail du produit Num {{ $pro->id }}</h1>
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">Designation:</dt>
                <dd class="col-sm-9">{{ $pro->designation }}</dd>

                <dt class="col-sm-3">Prix unitaire:</dt>
                <dd class="col-sm-9">{{ $pro->prix_u }}</dd>

                <dt class="col-sm-3">Quantité en stock:</dt>
                <dd class="col-sm-9">{{ $pro->quantite_stock }}</dd>

                <dt class="col-sm-3">Catégorie ID:</dt>
                <dd class="col-sm-9">{{ $pro->categorie_id }}</dd>
                @if ($pro->photo)
                <img src="{{asset('storage/'. $pro->photo)}}" style="width: 200px" alt="photo du produit">
                @else
                    <p>no photo available</p>
                @endif
            </dl>
        </div>
    </div>
@endsection


