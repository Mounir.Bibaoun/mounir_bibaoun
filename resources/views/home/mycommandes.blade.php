@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

<style>
    h1 {
        text-align: center;
        margin-bottom: 30px;
        color: #3498db; /* Blue color for the heading */
    }

    #tbl {
        width: 80%;
        margin: 0 auto;
        border-collapse: collapse;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        background-color: #ecf0f1;
    }

    th, td {
        border: 1px solid #bdc3c7; 
        padding: 15px;
        text-align: center;
    }

    th {
        background-color: #3498db; 
        color: #ffffff;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .total-row {
        font-weight: bold;
    }

    .action-btn {
        background-color: #2e4ecc; 
        color: #ffffff;
        padding: 8px 12px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .action-btn:hover {
        background-color: #9ea3a0; /* Darker green color on hover */
    }
</style>

<h1>Commandes :</h1>

<table id="tbl">
    <tr>
        <th>Id</th>
        <th>Date/Time</th>
        <th>Client ID</th>
        <th>Prix Total</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    @foreach ($mycommandes as $id => $item)
        <tr>
            <td>{{ $id }}</td>
            <td>{{ $item['date_time'] }}</td>
            <td>{{ $item['client_id'] }}</td>
            <td>{{ $item['prix_total'] }} MAD</td>
            <td>{{ $item['description'] }}</td>

        </tr>
    @endforeach
    <tr class="total-row">
        <th colspan="4">Total</th>
        <td colspan="2">{{ $item['prix_total'] }} MAD</td>
    </tr>
</table>

@endsection
