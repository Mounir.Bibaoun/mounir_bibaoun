@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

<style>
    .catalogue {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }

    .item {
        width: 300px;
        border: 1px solid #ddd;
        border-radius: 8px;
        overflow: hidden;
        margin: 10px;
        transition: transform 0.3s ease-in-out;
        text-align: center;
        background-color: #fff;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    }

    .item:hover {
        transform: scale(1.05);
    }

    .item img {
        width: 100%;
        height: 200px;
        object-fit: cover;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
    }

    .item-content {
        padding: 15px;
    }

    .item p {
        margin: 5px 0;
        font-size: 14px;
        color: #555;
    }

    .item-title {
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 10px;
        color: #333;
    }

    .btn-primary {
        background-color: #007bff;
        border-color: #007bff;
        color: #fff;
        padding: 8px 16px;
        border-radius: 4px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .btn-primary:hover {
        background-color: #0056b3;
        border-color: #0056b3;
    }

    .quantity-input {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .quantity-input input {
        width: 40px;
        text-align: center;
        margin: 0 5px;
        padding: 5px;
        border: 1px solid #ddd;
        border-radius: 4px;
        font-size: 14px;
    }

    .quantity-btn {
        background-color: #28a745;
        color: #fff;
        border: none;
        padding: 5px 10px;
        border-radius: 4px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .quantity-btn:hover {
        background-color: #218838;
    }
</style>

<div class="container my-4">
    <h1 class="mb-4">Liste des categories</h1>

    <div class="catalogue">
        @foreach ($produits as $item)
        <div class="item">
            <!-- Display product image if available -->
            @if (isset($item->photo))
            <img src="{{ asset('storage/' . $item->photo) }}" alt="{{ $item->designation }}">
            @endif
            <div class="item-content">
                <p class="item-title">{{ $item->designation }}</p>
                <p>Prix : {{ $item->prix_u }} MAD</p>

                @if ($item->quantite_stock == 0)
                <p class="text-danger">En rupture de stock</p>
                @else
                <p>En stock : {{ $item->quantite_stock }}</p>
                <div class="quantity-input">
        
                    <input type="number" name="qte" id="qte_{{ $item->id }}" min="1" max="{{ $item->quantite_stock }}" class="form-control">
                    
                </div>
                <form action="{{ route('home.add', ['id' => $item->id]) }}" method="POST">
                    @csrf
                    <input type="hidden" name="qte_hidden" id="qte_hidden_{{ $item->id }}" value="1">
                    <input type="submit" class="btn btn-primary mt-2" value="Acheter">
                </form>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection
