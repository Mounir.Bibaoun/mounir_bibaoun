<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\Models\Client;
use App\Models\Commande;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        
        $produits=Produit::all([ 'id','designation','prix_u','quantite_stock','photo']);
        return view('home.index',compact('produits'));
    }
    public function add(Request $request,$id){
        $qte=$request->input('qte',1);
         $produit=Produit::find($id);
        $panier=$request->session()->get('panier',[]);
        if(isset($panier[$id])){
            $panier[$id]['qte']=$qte;
        }else{
        $panier[$id]=[
            'qte'=>$qte,
             'produit'=>$produit
        ];
    }
    
    $request->session()->put('panier',$panier);
    return redirect()->back();
        
    }
    public function show_panier(Request $request){
        $panier=$request->session()->get('panier',[]);
        $tot=0;
        foreach($panier as $id=>$item){
            $tot+=$item['qte']*$item['produit']->prix_u;
        }
        return view('home.panier',compact('panier','tot'));
    }
    public function delete(Request $request,$id){
        $panier=$request->session()->get('panier',[]);
        unset($panier[$id]);
        $request->session()->put('panier',$panier);
        return redirect()->back();
    }
    public function clear(Request $request ){

        $request->session()->forget('panier');
        // return redirect()->back();
        return redirect()->route('home.index');

    }
    public function commander(Request $request){
        $panier=$request->session()->get('panier',[]);
        $tot=0;
        
        
        foreach($panier as $id=>$item){
            $tot+=$item['qte']*$item['produit']->prix_u;
            
        }
        
        
        return view('home.commander',compact('tot'));
    }
    public function storeClientCommande(Request $request){
        
       //Categorie::create($request->all());
       $clientdata = $request->validate([
        'nom'=>'required',
        'prenom'=>'required',
        'tele'=>'required',
        'ville'=>'required',
        'adresse'=>'required'
    ]);
    
       
        $client = Client::create($clientdata);
    
    $client_id = $client->id;
    $client_name = $client->nom . ' ' . $client->prenom;
    $date_time = now();
    $commandedata=$request->validate([
        'description'=>'required',
        'prix_total'=>'required'
    ]);
    $commandedata['client_id'] = $client_id;
    $commandedata['date_time'] = $date_time;
    Commande::create($commandedata);
    $mycommandes=Commande::all();
    return view('home.mycommandes',compact('mycommandes'));
      
    
    
    
    //return  redirect()->route('home.panier');

    }
    
}