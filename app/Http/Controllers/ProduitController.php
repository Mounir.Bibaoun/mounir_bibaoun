<?php

namespace App\Http\Controllers;
use App\Models\produit;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
    
       
        // return view("produits.index",compact('produits'));
       
        $designation = $request->query('designation');
        $prix_min = $request->query('prix_min');
        $prix_max = $request->query('prix_max');
        $categorie_id = $request->query('categorie_id');
        $query = Produit::query();
        if ($designation) {
            $query->where('designation', 'like', '%' . $designation . '%');
        }
        if($categorie_id){
            $query->where('categorie_id', '=', $categorie_id );
        }
    
        if ($prix_min) {
            $query->where('prix_u', '>=', $prix_min);
        }
        if ($prix_max) {
            $query->where('prix_u', '<=', $prix_max);
        }
    
        $nbr_produits =count($query->get()) ;
        $produits=Produit::paginate(15);
       
         return view("produits.index",compact('produits','designation','prix_min','prix_max','categorie_id','nbr_produits'));
       
       
    
    }
        
        
   

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('produits.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            'designation'=>'required|unique:categories,designation',
            'prix_u'=>'required',
            'quantite_stock'=>'required',
            'categorie_id'=>'required',
            'photo'=>'required|image|mimes:png,jpg,jpeg,svg|max:2048',
        ]);
        
        $data['photo'] = $request -> file('photo')->store('produit','public');
        Produit::create($data); 
        return  redirect()->route('produits.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $pro=Produit::find($id);
        
        return view('produits.show')->with("pro",$pro);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pro=Produit::find($id);
        return view('produits.edit',compact('pro'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data=$request->validate([
            'designation'=>'required|unique:produits,designation,'.$id,
            'prix_u'=>'required',
            'quantite_stock'=>'required',
            'categorie_id'=>'required',
            'photo'=>'required|image|mimes:png,jpg,jpeg,svg|max:2048',
        ]);
        $data['photo'] = $request -> file('photo')->store('produit','public');
        $pro=Produit::find($id);
        $pro->update($data);
        return redirect()->route('produits.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Produit::destroy($id);
        return  redirect()->route('produits.index');
    }

    
}
